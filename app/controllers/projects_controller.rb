class ProjectsController < ApplicationController

	before_action :authorized

	def new
		@project = Project.new
	end
	
	def create
		@project = Project.new(project_params)
		if @project.save
      redirect_to project_path(@project)
    else
      flash[:errors] = @project.errors.full_messages
      redirect_to new_project_path
    end
	end
	
	def show
		@project = Project.find(params[:id])
	end
	
	def edit
    @project = Project.find(params[:id])
  end

  def update
    @project = Project.find(params[:id])
    if @project.update_attributes(project_params)
      redirect_to project_path(@project)
    else
      flash[:errors] = @project.errors.full_messages
      redirect_to edit_project_path(@project)
    end
  end
	
	def destroy
		@project = Project.find(params[:id])
		if @project.destroy
			redirect_to dashboard_path
		else
			flash[:errors] = @project.errors.full_messages
			redirect_to project_path(@project)
		end
	end
	
	private
	
	def project_params
		params.require(:project).permit(:name, :status, :description)
	end
	
end
