class CommentsController < ApplicationController

	before_action :authorized
	
	def create
		@comment = Comment.new(comment_params)
		respond_to do |format|
			if @comment.save
				format.json { render json: { status: :created } }
			else
				format.json { render json: @comment.errors,
					status: :unprocessable_entity }
			end
		end
	end
	
	private
	
	def comment_params
		params.require(:comment).permit(:issue_id, :commenter_id, :content)
	end

end
