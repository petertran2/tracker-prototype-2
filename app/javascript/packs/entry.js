import TurbolinksAdapter from 'vue-turbolinks'
import Vue from 'vue/dist/vue.esm'
import VueResource from 'vue-resource'

Vue.use(TurbolinksAdapter)
Vue.use(VueResource)

document.addEventListener('turbolinks:load', () => {
	Vue.http.headers.common['X-CSRF-Token'] =
		document.querySelector('meta[name="csrf-token"]').getAttribute('content')
	
	let element = document.getElementById('vuejs')
	if (element !== null) {
		let app = new Vue({
		  el: element,
		  data: function() {
		  	return {
		  		comments: JSON.parse(element.dataset.comments),
		  		newComment: JSON.parse(element.dataset.newComment),
		  		error: null
		  	}
		  },
		  methods: {
		  	addComment: function() {
		  		this.$http.post('/comments', { comment: this.newComment })
		  			.then(response => {
		  				window.location.reload()
		  			}, response => {
		  				this.error = 'Comment ' + JSON.parse(response.bodyText).content[0]
		  			})
		  	},
		  	creationTime: function(comment) {
		  		const date = new Date(comment.created_at).toLocaleDateString(undefined
		  			, { year: 'numeric', month: 'long', day: 'numeric' })
		  		const time = new Date(comment.created_at).toLocaleTimeString()
		  		return date + ' ' + time
		  	}
		  }
		})
	}
})
