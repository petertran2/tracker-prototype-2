class Issue < ApplicationRecord
	validates :project_id, :reporter_id, :subject, presence: true
	PRIORITIES = %w(Low Medium High Urgent)
	validates :priority, inclusion: { in: PRIORITIES }
	STATUSES = %w(New Assigned Feedback Acknowledged Resolved Closed)
	validates :status, inclusion: { in: STATUSES }
	
	belongs_to :project
	belongs_to :reporter, primary_key: :id, foreign_key: :reporter_id,
		class_name: :User
	belongs_to :assignee, primary_key: :id, foreign_key: :assignee_id,
		class_name: :User, optional: true
	has_many :comments, dependent: :destroy
end
