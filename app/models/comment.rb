class Comment < ApplicationRecord
	validates :issue_id, :commenter_id, :content, presence: true
	
	belongs_to :issue
	belongs_to :commenter, primary_key: :id, foreign_key: :commenter_id,
		class_name: :User
end
