class User < ApplicationRecord
	has_secure_password
  validates :name, presence: true
  validates :email, presence: true, uniqueness: { case_sensitive: false }
  validates :password, :password_confirmation, length: { minimum: 1, allow_nil: true }
  
  has_many :reported_issues, primary_key: :id, foreign_key: :reporter_id,
  	class_name: :Issue, dependent: :destroy
  has_many :assigned_issues, primary_key: :id, foreign_key: :assignee_id,
  	class_name: :Issue, dependent: :destroy
  has_many :comments, primary_key: :id, foreign_key: :commenter_id,
  	class_name: :Comment, dependent: :destroy
end
