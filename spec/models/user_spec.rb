require 'rails_helper'

RSpec.describe User, type: :model do

  let(:user) { User.new(name: 'test', email: 'test@email.com',
  	password: '123', password_confirmation: '123') }
  	
  it 'is valid with valid credentials' do
  	expect(user).to be_valid
  end
  
  it 'is invalid with missing name' do
  	user.name = nil
  	expect(user).to_not be_valid
  end
  
  it 'is invalid with missing email' do
  	user.email = nil
  	expect(user).to_not be_valid
  end
  
  it 'is invalid with missing password' do
  	user.password = nil
  	expect(user).to_not be_valid
  end
  
end
