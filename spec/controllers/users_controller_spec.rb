require 'rails_helper'

RSpec.describe UsersController, type: :controller do
	describe "GET #new" do
    it "assigns @user" do
    	get :new
      expect(assigns(:user)).to be_a_new(User)
    end
  end
end
