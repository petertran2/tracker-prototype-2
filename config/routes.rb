Rails.application.routes.draw do
  root 'home#index'
  
  get "/signup", to: "users#new"
  resources :users, only: [:create]
  
  get "/login", to: "sessions#new"
  post "/sessions", to: "sessions#create"
  delete "/sessions", to: "sessions#destroy"
  
  get '/dashboard', to: 'dashboard#index'
  
  resources :projects, except: [:index]
  resources :issues, except: [:index]
  resources :comments, only: [:create]
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
