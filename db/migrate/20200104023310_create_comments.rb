class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.integer :issue_id, null: false
      t.integer :commenter_id, null: false
      t.text :content, null: false
      t.timestamps
    end
    add_index :comments, :issue_id
    add_index :comments, :commenter_id
  end
end
