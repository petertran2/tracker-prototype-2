class CreateIssues < ActiveRecord::Migration[5.2]
  def change
    create_table :issues do |t|
      t.integer :project_id, null: false
      t.integer :reporter_id, null: false
      t.integer :assignee_id
      t.string :priority, default: 'Medium'
      t.string :status, default: 'New'
      t.string :subject, null: false
      t.text :description
      t.date :due_date
      t.timestamps
    end
    add_index :issues, :project_id
    add_index :issues, :reporter_id
    add_index :issues, :assignee_id
  end
end
